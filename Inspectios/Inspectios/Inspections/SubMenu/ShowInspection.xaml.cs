﻿using Classes.Vehicle;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Inspections.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowInspection : ContentPage
	{
		public ShowInspection ()
		{
			InitializeComponent ();
		}

        //Creating a list to insert all inspections from the db into the list so we can use the list to put in the list view
        List<Inspection> inspections = new List<Inspection>();

        private void InspectionsLoadBtt_Clicked(object sender, EventArgs e)
        {
            //Variable date time with the value of today date
            DateTime today = DateTime.Today;

            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT ID, I_DATE, I_VEHICLE FROM INSPECTIONS WHERE I_VEHICLE=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    cmm.Parameters.AddWithValue("@ID", InspectionVehicleIdEntry.Text);

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Checking if the inspection is already done or not
                            if (reader.GetDateTime(1) >= today)
                            {
                                //adding to the list the inspections to display in the list view
                                inspections.Add(new Inspection(reader.GetInt32(0), reader.GetDateTime(1)));
                            }
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Passing the list of inspections to the itemsource of the list view
            ListViewInspections.ItemsSource = inspections;

            //Display alert that the clients are loaded to the screen
            DisplayAlert("Alert", "Inspections Are loaded", "Thank you");
        }
    }
}