﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Vehicle
{
    public class Inspection
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public DateTime DateOfTheInspection { get; set; }
        public static int Count { get; set; }

        public Inspection(int id, DateTime dateOfTheInspection)
        {
            this.Id = id;
            this.DateOfTheInspection = dateOfTheInspection;
            Count++;
        }
    }
}
