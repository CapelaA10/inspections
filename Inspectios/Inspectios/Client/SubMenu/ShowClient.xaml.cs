﻿using Classes.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Client.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowClient : ContentPage
	{
		public ShowClient()
		{
			InitializeComponent();
		}

        //Creating a list to put all info from the db to the list
        List<Classes.Client.Client> clients = new List<Classes.Client.Client>();

        //Loading clients to a list to display in the list view
        public void LoadClients()
        {
            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT ID, C_NAME, C_LASTNAME, C_ADRESS, C_ACTIVE, C_EMAIL, C_CELLPHONENUMBER FROM CLIENTS";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Check if the user is active or not 
                            if (reader.GetBoolean(4) == true)
                            {
                                //Creating the users with the new info about
                                clients.Add(new Classes.Client.Client(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetBoolean(4), reader.GetString(5), reader.GetString(6)));
                            }
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Passing the list of clients to the itemsource of the list view
            ListViewClients.ItemsSource = clients;

            //Display alert that the clients are loaded to the screen
            DisplayAlert("Alert", "Clients Are loaded", "Thank you");
        }

        private void RefreshClients_Clicked(object sender, EventArgs e)
        {
            LoadClients();
        }
    }
}