﻿using Inspectios.Client;
using Inspectios.Employ;
using Inspectios.Inspections;
using Inspectios.SendEmail;
using Inspectios.Vehicle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.MainMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMenuP : ContentPage
	{
		public MainMenuP ()
		{
			InitializeComponent ();
		}

        private void ClientsBttMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ClientsManagment());
        }

        private void VehiclesBttMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new VehicleManagment());
        }

        private void InspectionsBttMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InspectionManagment());
        }

        private void EmploysBttMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new EmployManagment());
        }

        private void SendEmailsBttMenu_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SendEmailP());
        }
    }
}