﻿using Inspectios.MainMenu;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Client.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UpdateClient : ContentPage
	{
		public UpdateClient ()
		{
			InitializeComponent ();
		}

        private void UpdateClientBtt_Clicked(object sender, EventArgs e)
        {
            //Try catch to check if it is a error in the db connection
            try
            {
                //Connection string to connect to the db 
                string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "UPDATE CLIENTS SET C_ACTIVE=1, C_EMAIL=@EMAIL, C_ADRESS=@ADRESS, C_CELLPHONENUMBER=@CELLNUMBER WHERE ID=@ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Add parameters so the query know what is getting add
                    cmm.Parameters.AddWithValue("@ID", ClientIdEntry.Text);
                    cmm.Parameters.AddWithValue("@EMAIL", ClientEmailEntry.Text);
                    cmm.Parameters.AddWithValue("@ADRESS", ClientAdressEntry.Text);
                    cmm.Parameters.AddWithValue("@CELLNUMBER", ClientCellPhoneEntry.Text);

                    //Execute
                    cmm.ExecuteNonQuery();

                    //Closing connection
                    cnn.Close();

                    //Display info and getting back to the main menu menu
                    DisplayAlert("Client", "is updated", "Thank you");
                    Navigation.PushModalAsync(new MainMenuP());
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }
        }
    }
}