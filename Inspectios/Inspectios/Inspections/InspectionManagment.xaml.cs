﻿using Inspectios.Inspections.SubMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Inspections
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InspectionManagment : ContentPage
	{
		public InspectionManagment ()
		{
			InitializeComponent ();
		}

        private void ShowInspectionsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowInspection());
        }

        private void InsertInspectionsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertInspection());
        }

        private void UpdateInspectionsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateInspection());
        }
    }
}