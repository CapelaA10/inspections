﻿using Inspectios.Employ.SubMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Employ
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EmployManagment : ContentPage
	{
		public EmployManagment ()
		{
			InitializeComponent ();
		}

        private void ShowEmployBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowEmploy());
        }

        private void InsertEmployBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertEmploy());
        }

        private void UpdateEmployBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateEmploy());
        }

        private void DeleteEmployBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteEmploy());
        }
    }
}