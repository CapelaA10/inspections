﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Vehicle
{
    public class Vehicle
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Brand { get; set; }
        public string RegistrationPlate { get; set; }
        public string Category { get; set; }
        public string Model { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the vehicle</param>
        /// <param name="brand">Brand of the vehicle</param>
        /// <param name="registrationPlate">Registration Plate of the vehicle</param>
        /// <param name="category">Category of the vehicle</param>
        /// <param name="model">Model of the vehicle</param>
        public Vehicle(int id, string brand, string registrationPlate, string category, string model)
        {
            this.Id = id;
            this.Brand = brand;
            this.RegistrationPlate = registrationPlate;
            this.Category = category;
            this.Model = model;
            Count++;
        }
    }
}
