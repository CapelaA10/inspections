﻿using Inspectios.Client.SubMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Client
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ClientsManagment : ContentPage
	{
		public ClientsManagment ()
		{
			InitializeComponent ();
		}

        private void ShowClientsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowClient());
        }

        private void InsertClientsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertClient());
        }

        private void UpdateClientsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new UpdateClient());
        }

        private void DeleteClientsBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteClient());
        }
    }
}