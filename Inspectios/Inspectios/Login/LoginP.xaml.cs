﻿using Classes.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Data.SqlClient;
using Inspectios.MainMenu;

namespace Inspectios.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginP : ContentPage
    {
        //Creating a list to have all users in
        List<User> users = new List<User>();

        public LoginP()
        {
            InitializeComponent();
            LoadUsers();
        }

        //Button login clicked, check if the credentials are alright
        private void LoginBtt_Clicked(object sender, EventArgs e)
        {
            //Local Variables
            string username;
            string password;

            //Inicializating variables
            username = UserNameEntry.Text;
            password = PasswordEntry.Text;

            //Cicle for to check all users in the list 
            for (int i = 0; i < users.Count; i++)
            {
                //If to check if the password and username are right
                if (username == users[i].Username && password == users[i].Password)
                {
                    //Display alert
                    DisplayAlert("WELCOME", "YOU ARE IN", "THANK YOU");

                    //Going to the main menu page
                    Navigation.PushModalAsync(new MainMenuP());
                }
            }
        }

        //Load Users to a list from the db
        private void LoadUsers()
        {
            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch cheking if the is some error in the db connection or the commands
            try
            {
                //Using the sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Creating the string of sql command
                    string query = "SELECT ID, U_USERNAME, U_PASSWORD, U_ACTIVE FROM USERS";

                    //Creating a sql command in c#
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using data reader and creating a new reader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //While cicle to read all info in the table
                        while (reader.Read())
                        {
                            //If to check if the user is active or non active that means if the user is deleted or not
                            if (reader.GetBoolean(3) == true)
                            {
                                //Creating the users from the db with all info needed
                                users.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3)));
                            }
                        }
                    }
                }
            }
            catch
            {
                //Display alert to the user that something went wrong with the connection
                DisplayAlert("ERROR", "Something went wrong check your wifi connection", "TRY AGAIN");
            }
        }
    }
}