﻿using Inspectios.Vehicle.SubMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Vehicle
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VehicleManagment : ContentPage
	{
		public VehicleManagment ()
		{
			InitializeComponent ();
		}

        private void ShowVehicleBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ShowVehicle());
        }

        private void InsertVehicleBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertVehicle());
        }

        private void DeleteVehicleBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new DeleteVehicle());
        }
    }
}