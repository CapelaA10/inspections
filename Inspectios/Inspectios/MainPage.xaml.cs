﻿using Inspectios.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Inspectios
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void LoginBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginP());
        }
    }
}
