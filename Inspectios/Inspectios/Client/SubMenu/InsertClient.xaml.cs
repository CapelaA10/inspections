﻿using Inspectios.MainMenu;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Client.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InsertClient : ContentPage
	{
		public InsertClient ()
		{
			InitializeComponent ();
		}

        private void InsertClientBtt_Clicked(object sender, EventArgs e)
        {
            //Connection string to the db
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            
            //Try catch to see if the user have any error in the connection to the db
            try
            {
                //Connecting to the db
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "INSERT INTO CLIENTS (ID, C_NAME, C_LASTNAME, C_ADRESS, C_ACTIVE, C_EMAIL, C_CELLPHONENUMBER, C_USERID) VALUES ((SELECT MAX(ID)+1 FROM CLIENTS), @NAME, @LASTNAME, @ADRESS, 1, @EMAIL, @CELLPHONENUMBER, @IDUSER )";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Adding values to the command that is going to work in the db
                    cmm.Parameters.AddWithValue("@NAME", ClientNameEntry.Text);
                    cmm.Parameters.AddWithValue("@LASTNAME", ClientLastNameEntry.Text);
                    cmm.Parameters.AddWithValue("@ADRESS", ClientAdressEntry.Text);
                    cmm.Parameters.AddWithValue("@EMAIL", ClientEmailEntry.Text);
                    cmm.Parameters.AddWithValue("@CELLPHONENUMBER", ClientCellPhoneEntry.Text);
                    cmm.Parameters.AddWithValue("@IDUSER", ClientUserIdEntry.Text);

                    //Executing a non query command on the db
                    cmm.ExecuteNonQuery();

                    //Display alert to the user that the client is insert and getting back to the main menu
                    DisplayAlert("Client", "Is Inserted", "Thank You");
                    Navigation.PushModalAsync(new MainMenuP());

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                //Alert to the user
                DisplayAlert("ERROR", "Please check your wifi connection", "Thank you");
            }
        }
    }
}