﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Vehicle.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowVehicle : ContentPage
	{
		public ShowVehicle ()
		{
			InitializeComponent ();
		}

        //Creating a list to insert all vehicles from the db in it
        List<Classes.Vehicle.Vehicle> vehicles = new List<Classes.Vehicle.Vehicle>();

        //Btt cliecked event 
        private void ShowVehiclesBtt_Clicked(object sender, EventArgs e)
        {
            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT ID, V_BRAND, V_REGISTRATIONPLATE, V_CATEGORY, V_MODEL, V_ACTIVE FROM VEHICLE WHERE V_CLIENT = @ID";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Adding the value to search in the db 
                    cmm.Parameters.AddWithValue("@ID", ClientIdEntry.Text);

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //If the vehicle is active or deleted
                            if (reader.GetBoolean(5) == true)
                            {
                                //Adding the vehicles to the list 
                                vehicles.Add(new Classes.Vehicle.Vehicle(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
                            }
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }

                //Adding the list vehicles to the list view
                ListViewVehicles.ItemsSource = vehicles;
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Passing the list of vehicles to the itemsource of the list view
            ListViewVehicles.ItemsSource = vehicles;

            //Display alert that the clients are loaded to the screen
            DisplayAlert("Alert", "Vehicles Are loaded", "Thank you");
        }
    }
}