﻿using Classes.User;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.Employ.SubMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowEmploy : ContentPage
	{
		public ShowEmploy ()
		{
			InitializeComponent ();
		}

        //Creating a list to put all info from the db to the list
        List<User> users = new List<User>();

        //Loading clients to a list to display in the list view
        public void LoadUsers()
        {
            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Try catch to check if it is a error in the db connection
            try
            {
                //Connecting to DB
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {

                    //Command string
                    string command = "SELECT ID, U_USERNAME, U_PASSWORD, U_ACTIVE FROM USERS";

                    //Sql Command
                    SqlCommand cmm = new SqlCommand(command, cnn);

                    //Opening connection
                    cnn.Open();

                    //Using datareader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //Check and while was info in
                        while (reader.Read())
                        {
                            //Check if the user is active or not 
                            if (reader.GetBoolean(3) == true)
                            {
                                //Creating the users with the new info about
                                users.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3)));
                            }
                        }
                    }

                    //Closing connection
                    cnn.Close();
                }
            }
            catch
            {
                DisplayAlert("No WiFi connection", "Please turn on your WiFi to use the app", "OK");
            }

            //Passing the list of users to the itemsource of the list view
            ListViewUsers.ItemsSource = users;

            //Display alert that the users are loaded to the screen
            DisplayAlert("Alert", "Clients Are loaded", "Thank you");
        }

        private void RefreshEmploys_Clicked(object sender, EventArgs e)
        {
            LoadUsers();
        }
    }
}