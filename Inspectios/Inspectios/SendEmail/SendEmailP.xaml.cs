﻿using Classes.Vehicle;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inspectios.SendEmail
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SendEmailP : ContentPage
	{
		public SendEmailP ()
		{
			InitializeComponent ();
		}

        private void SendEmailBtt_Clicked(object sender, EventArgs e)
        {
            //Variable date time with the value of today date
            DateTime today = DateTime.Today;

            //Creating the connection string
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=inspectionsCenter;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //String to using in the email seending
            string clientEmail = "";
            string inspectionSubject = "Inspection";
            string body = "";

            //Connecting to DB
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {

                //Command string
                string command = "SELECT INSPECTIONS.ID ,INSPECTIONS.I_DATE, CLIENTS.C_EMAIL, VEHICLE.V_REGISTRATIONPLATE FROM CLIENTS INNER JOIN VEHICLE ON VEHICLE.V_CLIENT = CLIENTS.ID INNER JOIN INSPECTIONS ON INSPECTIONS.I_VEHICLE = VEHICLE.ID WHERE CLIENTS.ID = @ID; ";

                //Sql Command
                SqlCommand cmm = new SqlCommand(command, cnn);

                //Opening connection
                cnn.Open();

                //Adding parameters to the query
                cmm.Parameters.AddWithValue("@ID", IdClientEntry.Text);

                //Creating the start of the body
                body = "Hey sir\n" +
                       "This cars will have there inspection in the days :\n";

                //Using datareader
                using (SqlDataReader reader = cmm.ExecuteReader())
                {
                    //Check and while was info in
                    while (reader.Read())
                    {
                        //Getting the email of the client 
                        clientEmail = reader.GetString(2);

                        //Checking if the inspection is already done or not
                        if (reader.GetDateTime(1) >= today)
                        {
                            //Adding to the body the info about the cars and days of inspection
                            body += reader.GetString(3).ToString() + " " + reader.GetDateTime(1).ToString() + "\n";
                        }
                    }
                }

                //Finalazing the body
                body += "\n If you cant be present please contact us\n Thank you from your amazing team Inspection Center";

                //Closing connection
                cnn.Close();
                
                //Creating the mail message
                MailMessage mailMsg = new MailMessage();

                //Adding the reciver of the mail in this case the client
                mailMsg.To.Add(new MailAddress(clientEmail));

                //Saying where the email is from
                mailMsg.From = new MailAddress("inspectionscenterapp1@gmail.com", "Team Inspection Center");

                //Adding subject and body to the mail
                mailMsg.Subject = inspectionSubject;
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Plain)); 

                //Creating the smtp client adding the port and the host giving the credentials
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("azure_25d121931de3b48d06529d5b17d3d1de@azure.com", "28021989Pi");
                smtpClient.Credentials = credentials;

                //Sending the email
                smtpClient.Send(mailMsg);

                //Display the alert to the user so he knows that the email was sent
                DisplayAlert("Email", "Sended", "Thank You");
            }
        }
    }
}