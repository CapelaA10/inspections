﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.Client
{
    public class Client
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
        public string CellPhoneNumber { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer for the client
        /// </summary>
        /// <param name="id">Id of the client</param>
        /// <param name="name">Name of the client</param>
        /// <param name="lastName">Last Name of the client</param>
        /// <param name="adress">Adress of the client</param>
        /// <param name="active">If the client is active or not</param>
        /// <param name="email">Email of the client</param>
        /// <param name="cellPhoneNumber">Cell phone number of the client</param>
        public Client(int id, string name, string lastName, string adress, bool active, string email, string cellPhoneNumber)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Adress = adress;
            this.Active = active;
            this.Email = email;
            this.CellPhoneNumber = cellPhoneNumber;
            Count++;
        }
    }
}
