﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes.User
{
    public  class User
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer for the user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="active"></param>
        public User(int id, string username, string password, bool active)
        {
            this.Id = id;
            this.Username = username;
            this.Password = password;
            this.Active = active;
            Count++;
        }
    }
}
